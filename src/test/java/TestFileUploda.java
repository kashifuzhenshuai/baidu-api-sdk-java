import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.baidu.api.domain.FileItem;
import com.baidu.api.utils.HttpUtil;


/**
 * Copyright (c) 2011 Baidu.com, Inc. All Rights Reserved
 */

/**
 * 
 * @author chenhetong(chenhetong@baidu.com)
 *
 */
public class TestFileUploda {
    public static void main(String [] args) throws IOException{
        String url = "https://openapi.baidu.com/file/2.0/pic/pictures/upload";
        Map<String,String> params  = new HashMap<String, String>();
        params.put("access_token", "3.809de73be9e845f9643f668abe5e7544.2592000.1351406344.1528405415-294250");
        Map<String,FileItem> fileParams = new HashMap<String, FileItem>();
        FileItem fileValue = new FileItem("src/test/resources/R.png");
        fileParams.put("upload",fileValue);
        System.out.println(fileValue.getMimeType());
        String result=null;
        try {
            result = HttpUtil.uploadFile(url, params, fileParams, 1000, 1000);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(result);
    } 
    public static String getFileSuffix(byte[] bytes) {
        if (bytes == null || bytes.length < 10) {
            return null;
        }

        if (bytes[0] == 'G' && bytes[1] == 'I' && bytes[2] == 'F') {
            return "GIF";
        } else if (bytes[1] == 'P' && bytes[2] == 'N' && bytes[3] == 'G') {
            return "PNG";
        } else if (bytes[6] == 'J' && bytes[7] == 'F' && bytes[8] == 'I' && bytes[9] == 'F') {
            return "JPG";
        } else if (bytes[0] == 'B' && bytes[1] == 'M') {
            return "BMP";
        } else {
            return null;
        }
    }

}
